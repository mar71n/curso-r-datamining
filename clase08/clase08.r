# clase 08

# Cargar datos y libreria
#-----------------------------------------
# install.packages("igraph")
library(igraph)
datos <- data.frame(from=c("juan","juan","pedro","pedro","pedro","maria","jose"),
                    to =c("maria","jose","juan","jose","pepe","jose","pepe"))
# Crear objeto de grafico
#-----------------------------------------
grafo <- graph.data.frame(datos)
plot(grafo)
# Calcular relevancia
#-----------------------------------------
Relevancia <- page.rank(grafo)$vector


# install.packages("igraph")
library(igraph)
# dataset
#datos <- read.csv("https://www.dropbox.com/s/blugxmyhty6lkeq/Twitter_network_R.csv?dl=1")
datos <- read.csv("Twitter_network_R.csv")
# crea objeto de grafo y grafico
grafo <- graph.data.frame(datos[1:50,])
str(datos)
str(grafo)
attr(grafo,"class")
attr(grafo[1],"names")
sapply(1:10, function(i){length(attr(grafo[i],"names"))})
attr(grafo[1],"env")
attr(grafo[1],"graph")
grafoT <- graph.data.frame(datos)
sapply(1:10, function(i){length(attr(grafoT[i],"names"))})
plot(grafo,layout=layout.fruchterman.reingold, vertex.size=8,
     vertex.label.dist=0.4, vertex.color="red", edge.arrow.size=0.5)
# calcula page rank
pr.tmp <- data.frame(pr.value= page.rank(grafo)$vector)
pr.tmpT <- data.frame(pr.value= page.rank(grafoT)$vector)
pr <- data.frame(pr.desc=row.names(pr.tmp), pr.val = pr.tmp[,1])
head(pr[order(pr$pr.val,decreasing = T),])
# grafica objeto de grafo usando usado valor page.rank
V(grafo)$label.cex = 0.6 + pr$pr.val*3 # tamaño de letra usando PageRank
plot(grafo, vertex.size=pr$pr.val*100,
     vertex.label.dist=0.4, vertex.color="red", edge.arrow.size=0.3)


### ensamble
# Carga Package y Set de datos
# install.packages("adabag")
library(adabag)
#url.data <- 'https://www.dropbox.com/s/3u8fq8tl67iz88q/churn.csv?dl=1'
url.data <- 'churn.csv'
data0 <- read.csv(url.data)
datos <- data0[, c(4,7,16,19,17,20)]
# Divide set en Train y Test
n <- nrow(datos)
muestra <- sample(x = n, size = n * .3)
Train <- datos[-muestra,]
Test <- datos[muestra,]
# Crea modelo
modelo_ada <- boosting(churn ~ ., data=Train,
                       mfinal=100 # iteraciones
)
# Prediccion en TEST
Prediccion <- predict(modelo_ada, Test)$class
(MC <- table(Test[, "churn"],Prediccion) )

(precision_ <- MC[2,2] / (MC[2,2] + MC[1,2]))
(recall_ <- MC[2,2] / (MC[2,2] + MC[2,1]))
(accuracy <- (MC[2,2] + MC[1,1]) / sum(MC) )

### SVM
# Carga Package y Set de datos
library(e1071);
#url.data <- 'https://www.dropbox.com/s/3u8fq8tl67iz88q/churn.csv?dl=1'
url.data <- 'churn.csv'
data0 <- read.csv(url.data)
datos <- data0[, c(4,7,16,19,17,20)]
# Divide set en Train y Test
n <- nrow(datos)
muestra <- sample(x = n, size = n * .3)
Train <- datos[-muestra,]
Test <- datos[muestra,]
# Crea modelo
modelo_svm <- svm(churn ~ ., data=Train, kernel = "radial")
modelo_svm_s <- svm(churn ~ ., data=Train, kernel = "sigmoid")
# Prediccion en TEST
Prediccion <- predict(modelo_svm,Test)
Prediccion_s <- predict(modelo_svm_s,Test)
(MC <- table(Test[, "churn"],Prediccion) )
(precision_ <- MC[2,2] / (MC[2,2] + MC[1,2]))
(MC <- table(Test[, "churn"],Prediccion_s) )
(precision_ <- MC[2,2] / (MC[2,2] + MC[1,2]))

#### overitting

library(randomForest)
# datos
df <- data.frame(x = runif(1000),
                 y = ifelse(runif(1000)>0.5, "si", "no")
)
# modelo
m <- randomForest(y ~ x, data=df, ntree=500)
# prediccion
table(df$y, predict(m, df))

