# clase 5
# Regresión lineal múltiple sobre mtcars mediante las siguientes características de
# los autos: millas por galón (mpg), cilindros (disp), caballos de fuerza (hp),
# peso del auto (wt).
# Armar el dataset
input <- mtcars[,c("mpg","disp","hp","wt")]
summary(input)
cor(input)
# Crear el modelo
modelo <- lm(mpg~disp+hp+wt, data=input)
print(modelo)
summary(modelo)
# Graficar
plot(modelo) # Muestra los 4 gráficos
predict(modelo, input) # Interpretar!
str(predict(modelo, input))
compm <- cbind(input$mpg, predict(modelo, input))
compm_df <- as.data.frame(compm)
names(compm_df) <- c("input","modelo")
# Extraer los valores de los coeficientes para luego crear la ecuación
# correspondiente: Y = a+Xdisp*x1+Xhp*x2+Xwt*x3
a <- coef(modelo)[1]
Xdisp <- coef(modelo)[2]
Xhp <- coef(modelo)[3]
Xwt <- coef(modelo)[4]


# carga datos desde dropbox
# datos.url <- "https://www.dropbox.com/s/r6panhu58ps38d2/train_titanic.csv?dl=1"
# data0 <- read.csv(datos.url)
data0 <- read.csv("titanic.csv")
summary(data0)
datos <- within(data0, rm(Name,PassengerId,Ticket,Cabin,Age))
# Divide set en Train y Test
set.seed(1)
n <- nrow(datos)
muestra <- sample(x = n, size = n * .3)
Train <- datos[-muestra,]
Test <- datos[muestra,]
# Crea modelo predictivo
Modelo_glm <- glm(Survived ~., Train, family = binomial(link = "logit"))
# Hace predicción y crea matriz de confusión
Prediccion <- predict(Modelo_glm, newdata = Test, type = "response")
Prediccion <- round(Prediccion) # Umbral = 0.5. ¿Cómo alterarlo?
Prediccion <- as.numeric(Prediccion >= 0.4)
(MC <- table(Test[, "Survived"],Prediccion))
# Calcular Precision y Recall. Tip:
MC[1,1] # 126
(precision_ <- MC[2,2] / (MC[2,2] + MC[1,2]))

data1 <- read.csv("churn.csv")
datos <- data1
cor(data1)
set.seed(1)
n <- nrow(datos)
muestra <- sample(x = n, size = n * .3)
Train <- datos[-muestra,]
Test <- datos[muestra,]
# Crea modelo predictivo
Modelo_glm <- glm(churn ~., Train, family = binomial(link = "logit"))
# Hace predicción y crea matriz de confusión
Prediccion <- predict(Modelo_glm, newdata = Test, type = "response")
Prediccion <- round(Prediccion) # Umbral = 0.5. ¿Cómo alterarlo?
Prediccion <- as.numeric(Prediccion >= 0.4)
(MC <- table(Test[, "churn"],Prediccion))
# Calcular Precision y Recall. Tip:
MC[1,1] # 126
(precision_ <- MC[2,2] / (MC[2,2] + MC[1,2]))
(recall_ <- MC[2,2] / (MC[2,2] + MC[2,1]))
(accuracy <- (MC[2,2] + MC[1,1]) / sum(MC) )

