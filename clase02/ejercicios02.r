# 2. �Las siguientes expresiones devuelven TRUE o FALSE?
#a.
x <- 12
x > 5 & x < 15
#b.
x <- 12
x > 5 | x > 15

# 3. Crear vectores y matrices con diferentes elementos, relacionados entre s�. Luego aplicar
# las siguientes funciones sobre los objetos creados, con el objetivo de describirlos mejor:
v1 <- c(1,2,3,4,5,6,7)
names(v1) <- c("Do","Lu","Ma","Mi","Ju","Vi","Sa")
v1
length(v1)
class(v1)
attributes(v1)
 
m1 <- matrix(1:9, nrow = 3, ncol = 3)
names(m1)
m1
dimnames(m1)
length(m1)
class(m1)
attributes(m1)

#4. Calcular cu�ntos valores mayor a 10 existen en el vector �a�
a <- c(1,4,77,11,21,0,2,8)
sum(a > 10)

# 5. Crea una funci�n que calcule el valor absoluto de un n�mero
f <- function(x) {
  if(x > 0){
    x
  } else {
    -x
  }
}

f(2.3)
f(-2.6)
abs(-2.6)

# 6. Desarrollar un programa en R para manipular datos obtenidos de un archivo externo.
df.empleados <- read.csv("datos.csv")
nrow(df.empleados)
ncol(df.empleados)
head(df.empleados)
names(df.empleados)
str(df.empleados)

# d. Escribir comandos y sentencias para responder las siguientes preguntas:
# i. �Cu�l es el sueldo m�ximo entre todos los empleados de la empresa?
sueldo.max <- max(df.empleados[["sueldo"]])

# ii.�Cu�l es el empleado que tiene el mayor sueldo? Mostrar el registro
# completo de dicho empleado.
df.empleados[which(df.empleados$sueldo == sueldo.max),]

# iii. �Qui�nes trabajan en el �rea de IT y qu� caracter�sticas tienen?
df.empleados[which(df.empleados[["area"]] == "IT"), ]
df.empleados[match(df.empleados[["area"]],"IT"), ]

# iv. �Cu�les empleados de IT tienen un sueldo mayor a 600?
df.empleados[which(df.empleados[["area"]] == "IT" & df.empleados[["sueldo"]] > 600), ]
attach(df.empleados)
df.empleados[which(area == "IT" & sueldo > 600), ]

# v. �Qu� empleados comenzaron a trabajar a partir del 2014? Guardar el
# resultado en un dataframe y luego almacenarlo en un nuevo archivo
# empleados_recientes.csv? . Por �ltimo, leer dicho archivo y mostrar su contenido.
str(df.empleados)
empleados_recientes <- df.empleados[ gsub("([0-9]{4})(.*)","\\1",df.empleados[["fecha_comienzo"]] ) >= 2014 , ]
write.csv(empleados_recientes, "empleados_recientes.csv", row.names = FALSE)