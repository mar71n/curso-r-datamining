help("+")
?"+"

length.out
seq(1, 10, length.out = 5)
seq(1, 10, 5)
?seq

a <- 0
mifuncion <- function(){
  a <- 3
  c = 7
  b <<- 5
  pwa <- {"1234"}
  print(ls())
  a
}

mifuncion()
as.double("3.14")
?as.double

gsub(",",".","3,14")

precio <- c(sample(1:7,20, replace=TRUE))
names(precio) <- c("uno","uno")
precio["uno"]

iris

# esto
mi_v1 <- matrix(1:9, nrow=3, ncol=3)

iris[c(1,3,5)]
str(precio)


#' @title
#' Datos de las ventas
#' @description
#' los datos de las venta de la empresa
irisventas <- data.frame(Cliente = c("Juan","Pedro","Maria","Susana"),
                     Edad = c(20,30,80,40),
                     Cantidad = c(5,11,10,15),
                     Sucursal = c("Norte", "Sur", "Este", "Norte"))


ventas[ventas$Cantidad > ventas[ventas$Cliente=="Maria","Cantidad"], ]

ventas[ventas$Cantidad > ventas[ventas$Sucursal=="Norte","Cantidad"], ]


25 > c(5,30)

x <- c(5, 12, 13)
for (n in x) {
  print(n^2)
}


apply(1:3, 2, function(n,a){print(a[n,1]^2)}, as.matrix(x))


print(x^2)

f <- function(x){
  p50 <- x * .50
  p25 <- x * .25
  list(p50, p25)
}
f(10)

df <- iris

a <- c(1,2,3,4)
b <- c(2,6)

unique(a,b)
?unique

ls(all.names = TRUE)
environmentName()
?environment()
globalenv()

edad <- c(18,19,20,21,22,23,24,25,26,27,28,29)
altura <- c(76.1,77,78.1,78.2,78.8,79.7,79.9,81.1,81.2,81.8,82.8,83.5)
datos <- data.frame(edad, altura)
plot(datos)
resultado <- lm(altura ~ edad, data = datos)
# resultado <- lm(altura ~., data = datos) # altura podría correlacionarse con varias  variables que tuviera el dataframe
abline(resultado)
print(summary(resultado))


getwd()
datos <- read.csv("edad_altura.csv")


pw <- {"1234"}
ls()

rm(x)
median(x = edad)
x
 
median(x <- edad)
x



# Para asignar a parametros con nombre SIEMPRE hay que usar = (NUNCA <-)
# sea la función:
probando <- function(par1, par2){
  par1 - par2
}
# = se restringe al scope de la funcion
# en cambio <- busca primero si existe una bariable en el environment padre
# si existe la asigna y si no existe la crea
# en estos ejemplos usa los nombres de los parametros (en el ámbito de la funcion)
probando(par1 = 2, par2 = 3)
probando(par2 = 3, par1 = 2)
# en este otro ejemplo par2 y par1 son variables del enviroment principal
# y los usa como primer y segundo parámetro
probando(par2 <- 3, par1 <- 2)

# install.packages("readxl")
library(readxl)
datosxls <- read_excel("datos.xls")
View(datosxls)
str(datosxls)


# Creacion de objetos
precio <- c(15,27,19,11,13,10,17)
names(precio)<-c("Lun","Mar","Mie","Jue","Vie","Sab","Dom")
names(precio)<-c("Lun","Mar","Mie","Dom","Vie","Sab","Dom")
aumento <- c(2,1,1,4,3,7,8)
# Primeros 2 días
precio[1:2]
#Lun Mar
#15 27
# Ultimo dia
precio[length(precio)]
#Dom
#17
precio[c("Sab","Dom")]
#Sab Dom
#10 17
# Todos los dias excepto Sab y Dom
# OjO match trae el primero que concuerda, si hay repetidos trae solo el primero.
# which en cambio trae todos
precio[-match(precio[c("Sab","Dom")],precio)]
precio[-which(names(precio) %in% c("Sab","Dom"))]
precio[which(names(precio) != "Sab" & names(precio) != "Dom")]
precio[names(precio) != "Sab" & names(precio) != "Dom"]
precio[!names(precio) %in% c("Sab","Dom")]
# Dias con precio menor que 15
precio[precio<15]
#Jue Vie Sab
#11 13 10
# Dia con maryor precio
precio[precio==max(precio)]
#Mar
#27
precio_aumento <- precio + aumento
print(precioaumento)
#Lun Mar Mie Jue Vie Sab Dom
#17 28 20 15 16 17 25
promedio_aumento <- mean(aumento)
print(promedio_aumento)

