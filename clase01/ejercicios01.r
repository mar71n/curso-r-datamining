## Introducci�n a Data Mining con R
## Ejercicios de pr�ctica - Clase 1
# Utilizar la consola R o el entorno de desarrollo R Studio para resolver los siguientes ejercicios
##Regresi�n Lineal
## 1
# la edad explica la altura

edad <- c(18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29)
altura <- c(76.1, 77, 78.1, 78.2, 78.8, 79.7, 79.9, 81.1, 81.2, 81.8, 82.8, 83.5)

plot(edad , altura)

# altura ~ edad : altura "explicada" por edad
modeloea <- lm(altura ~ edad)

plot(edad , altura, col="blue")
abline(modeloea, col="red")
par(new=TRUE)
plot(predict(modeloea), col="green")

summary(modeloea)

altura_ <- function(edad){
  modeloea[["coefficients"]][2] * edad + modeloea[["coefficients"]][1]
}

# altura = edad * 0.635 + 64.928

?lm


## 2

# https://data.buenosaires.gob.ar/dataset/terrenos-valor-de-oferta
terrenos <- read.csv2("precio-de-terrenos-2017.csv", stringsAsFactor = TRUE)
View(terrenos)
summary(terrenos)
str(terrenos)
View(terrenos[,c(-1,-2,-3,-4,-5,-13,-14,-15)])
pairs(terrenos[,c(-1,-2,-3,-4,-5,-13,-14,-15)])

terrenos[["COMUNA"]]

# a. Identificar cu�les son las variables independientes X1, X2
# y cu�l es la variable dependiente Y.
str(terrenos)
# X1 y X2 pueden ser M2 y COMUNA
# y: U_S_M2

terrenos$dmetro2 <- terrenos[["DOLARES"]]/terrenos[["M2"]]
View(terrenos[,c("U_S_M2", "dmetro2")])

lm.terrenos <- lm(U_S_M2 ~ M2, data = terrenos)

plot(terrenos[["M2"]], terrenos[["U_S_M2"]])
abline(lm.terrenos)

summary(lm.terrenos)

lm.terrenos.fot <- lm(U_S_M2 ~ FOT, data = terrenos)

plot(terrenos[["FOT"]], terrenos[["U_S_M2"]])
abline(lm.terrenos.fot)

par(mfcol=c(2,2))
plot(lm.terrenos.fot)

lm.terrenos.m2.fot <- lm(U_S_M2 ~ M2 + FOT, data = terrenos)
par(mfcol=c(2,2))
plot(lm.terrenos.m2.fot)

summary(lm.terrenos.m2.fot)

library(housingData)
# install.packages("housingData")

head(housing)
summary(housing)

## obtengo del siguiente link un set de datos de casas
# https://www.kaggle.com/pradeeptripathi/predicting-house-prices-using-r

houses <- read.csv("./houses/train.csv")
head(houses)
str(houses)
houses$anios <- 2018 - houses[["YearBuilt"]]
summary(houses[,c("YearBuilt", "X1stFlrSF", "X2ndFlrSF", "SalePrice", "anios")])
houses$SupTot <- houses$X1stFlrSF + houses$X2ndFlrSF
summary(houses[,c("YearBuilt", "SupTot", "SalePrice", "anios")])

cor(houses[,c("YearBuilt", "X1stFlrSF", "X2ndFlrSF", "SalePrice", "anios")])
cor(houses[,c("YearBuilt", "SupTot", "SalePrice", "anios")])
cor(houses[,c("SupTot", "SalePrice", "anios")])

lm.houses <- lm(SalePrice ~ anios + SupTot, data = houses)
summary(lm.houses)
plot(lm.houses)

houses$Estimado <- houses$anios * (-1012.720) + houses$SupTot * 96.494 + 82579.172
houses$EstimadoPred <- predict(lm.houses)
houses$EstPorcent <- houses[["Estimado"]] / houses[["SalePrice"]]
houses$EstPorcent <- (houses[["SalePrice"]] - houses[["Estimado"]]) / houses[["SalePrice"]]
houses$EstPorcent <- (houses[["SalePrice"]] - houses[["EstimadoPred"]]) / houses[["SalePrice"]]
# me da un hist centrado en 0.5 ie. estoy estimando la mitad...
hist(houses[["EstPorcent"]], breaks=40)
View(houses[,c("SupTot", "SalePrice", "anios", "Estimado", "EstimadoPred")])

predict(lm.houses)


# aca no se ve claro...
par(col="green")
scatterplot3d(houses[["SupTot"]], houses[["anios"]], houses[["SalePrice"]] , type="h")
par(new=TRUE)
par(col="red")
scatterplot3d(houses[["SupTot"]], houses[["anios"]], houses[["Estimado"]] , type="h")
