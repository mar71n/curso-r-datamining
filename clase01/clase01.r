# 13/06/18

# regresion lineal

diametro <-  1:20
longitud <- diametro*pi+sample(-1:1,20, replace=TRUE)*(diametro/10)
longitud2 <- diametro*pi+sample(-25:25,20, replace=TRUE)*(diametro/10)

modelo <- lm(diametro ~ longitud)
plot(longitud,diametro)
abline(modelo)

plot(modelo)

cor(diametro, longitud)
cor(diametro, longitud2)
pairs(data.frame(diametro, longitud, longitud2))

#Crea representacion de ecuacion de regresion
y       <- all.vars(modelo$call)[1]
B_0     <- round(modelo$coef[1],2)
B_1     <- round(modelo$coef[2],2)
x_1     <- names(modelo$coef[2])
error   <- round(as.numeric(summary(modelo)[6]),2)

paste(y,"=",B_0,"+",B_1,"*",x_1,"+",error) #representacion de ecuacion

summary(modelo)

