# ----------------------------------------------------
# 1.  Usando el dataset mtcars , calcular el PCA e 
# identificar la proporción de la variabilidad descrita por los PC1 Y PC2  

summary(mtcars)
str(mtcars)
mtcars_pca <- prcomp(mtcars)
mtcars_pca$x
mtcars_pca_x <- as.data.frame(mtcars_pca$x)
str(mtcars_pca_x)
str(as.data.frame(mtcars_pca_x))
mtcars_pca_x[,"PC1"]
sd(mtcars_pca_x[,"PC1"])
# Variance de cada componente
var_pca <- apply(mtcars_pca_x,2,var)
#PC1          PC2          PC3          PC4          PC5          PC6          PC7          PC8 
#1.864127e+04 1.455276e+03 9.431143e+00 1.707336e+00 8.217172e-01 4.402868e-01 9.522105e-02 8.177335e-02 
#PC9         PC10         PC11 
#6.284913e-02 4.437424e-02 3.937199e-02 
var_pca[1:5]/sum(var_pca)
# PC1: 0.9269989
# PC2: 0.0723684

# 2.  Hacer cluster usando el algoritmo k-means con el dataset mtcars e identificar:  
# a.  Cuál es la cantidad  óptima de cluster
# Crea vector "SSE" Sum of Squares Error
SSE <- vector('numeric')
for (i in 1:9){
  SSE[i] <- sum(kmeans(mtcars, centers=i)$withinss)
}
# con sapply en lugar de for
sapply(1:9,function(i){SSE[i] <<-sum(kmeans(mtcars, centers=i)$withinss)})
# Gráfico de codo (Elbow plots)
plot(1:9, SSE, type="b",
     xlab="Cantidad de Clusters",
     ylab="Suma de error")
# en el grafico veo que 6 es el número óptimo de clusters
# aplico kmeans con 6 clusters
modelocars_km <- kmeans(mtcars, 6)
table(modelocars_km$cluster)
# para graficar los cluster uso la reduccion de variables del punto 1.
# lo paso a data.frame
mtcars_pca_x_df <- mtcars_pca_x
# le pego los cluster
mtcars_pca_x_df$cluster <- modelocars_km$cluster
# grafico
plot(mtcars_pca_x_df[,"PC1"], mtcars_pca_x_df[,"PC2"], col = mtcars_pca_x_df$cluster)

mtcarsA <- mtcars
str(modelocars_km$cluster)
mtcarsA$cluster <- modelocars_km$cluster
View(mtcarsA)

# b.  Cuál la suma de la distancia entre cada observación y su cluster  
# Distancia entre observaciones de cada cluster
modelocars_km$withinss # < b.
modelocars_km$tot.withinss
modelocars_km$tot.withinss == sum(modelocars_km$withinss)
# c.  Cuál es la suma de la distancia entre cluster
# Suma total de distancia entre observaciones
modelocars_km$betweenss # < c.
modelocars_km$totss - modelocars_km$tot.withinss # < c.
