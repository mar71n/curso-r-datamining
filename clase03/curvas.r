j <- 3
k <- 4
fx <- function(t,a,b){
  cos(a * t) + cos(b * t)^j
  #cos(t)
}
fy <- function(t,a,b){
  sin(a * t) + sin(b * t)^k
  #sin(t)
}

fx(1:20,1,5)
fy(1:20,1,5)

plot(fx(1:1500,3,1), fy(1:1500,3,1), col="green")
plot(fx(1:1500,7,1), fy(1:1500,7,1), col="green")
plot(fx(1:1000,1,80), fy(1:1000,80,80), col="blue")

par(new = TRUE)
plot(fx(1:1500,1,1) + 3, fy(1:1500,1,1))

