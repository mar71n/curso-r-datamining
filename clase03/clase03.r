x = c(1 , 3, 4, 2, 5, 4, 7, 6, 8)
y = c(2 , 4, 3, 5, 7, 6, 6, 7, 8)

df.datos <- data.frame(x,y)

mean(x)
mean(y)
sd(x)
sd(y)

summary(df.datos)

apply(df.datos, 2, mean)
apply(df.datos, 1, function(fila){fila[1] - mean(x)})
x - mean(x)
apply(df.datos, 1, function(fila){fila[2] - mean(y)})
y - mean(y)



str(iris)
# Generación de PCA
iris_pca <- prcomp(iris[,-5])
str(iris_pca)
# Data Frame con los Componentes
iris_pca_x <- as.data.frame(iris_pca$x)
# Resumen de PCA
summary(iris_pca)
# Importance of components:
# PC1 PC2 PC3 PC4
# Standard deviation 2.0563 0.49262 0.2797 0.15439
# Proportion of Variance 0.9246 0.05307 0.0171 0.00521
# Cumulative Proportion 0.9246 0.97769 0.9948 1.00000
# Standard deviation de PC1
sd(iris_pca_x$PC1)
# [1] 2.056269
# Variance de cada componente
apply(iris_pca_x, 2, var)
# PC1 PC2 PC3 PC4
# 4.22824171 0.24267075 0.07820950 0.02383509
# Proportion of Variance del PC1 = 0.9246, que es igual:
4.228/(4.228+0.2426+0.0782+0.0238)
# [1] 0.9246381
# PLOT
# por defecto plot() gráfica la varianza de los PC
plot(iris_pca)
# Graficar los PC1 y PC2
plot(iris_pca_x[, 1:2])
# agrega etiqueta al gráfico
text(iris_pca_x$PC1,iris_pca_x$PC2,
       labels = iris$Species,cex=.7,
       col = as.numeric(iris$Species))

modelo_km <- kmeans(iris[,-5], 3)
table(modelo_km$cluster)
summary(modelo_km)
modelo_km$withinss
modelo_km$totss

modpca_km <- kmeans(iris_pca_x[1:2], 3)
table(modpca_km$cluster)

iris2 <- iris
iris2$mi_cluster <- modelo_km$cluster
iris2$mi_clusterpca <- modpca_km$cluster

# Validación mi_cluster vs Species
table(iris2$Species, iris2$mi_cluster)
table(iris2$Species, iris2$mi_clusterpca)

# Crea vector "SSE" Sum of Squares Error
SSE <- vector('numeric')
# Ejecuta kmeans con diferentes K, desde 1 hasta 9
# y guarda distancia intra-cluster de cada ejecución en vector SSE
for (i in 1:9){
  SSE[i] <- sum(kmeans(iris[, -5], centers=i)$withinss)
}
apply(matrix(1:9,ncol=1), 2, function(i){sum(kmeans(iris[, -5], centers=i)$withinss)})
# Gráfico de codo (Elbow plots)
plot(1:9, SSE, type="b",
       xlab="Cantidad de Clusters",
       ylab="Suma de error")


# ----------------------------------------------------
summary(mtcars)
str(mtcars)
mtcars_pca <- prcomp(mtcars)
mtcars_pca$x
mtcars_pca_x <- mtcars_pca$x
str(mtcars_pca_x)
mtcars_pca_x[,"PC1"]
sd(mtcars_pca_x[,"PC1"])
# Variance de cada componente
var_pca <- apply(mtcars_pca_x,2,var)
#PC1          PC2          PC3          PC4          PC5          PC6          PC7          PC8 
#1.864127e+04 1.455276e+03 9.431143e+00 1.707336e+00 8.217172e-01 4.402868e-01 9.522105e-02 8.177335e-02 
#PC9         PC10         PC11 
#6.284913e-02 4.437424e-02 3.937199e-02 
var_pca[1:5]/sum(var_pca)
# 0.9269989 
