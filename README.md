# Clase 1
Presentacion
Data Mining
## Algoritmos supervisados - no supervisados
### Supervisados:  
  * Clasificacion (prediccion) (variable discreta)  
     * Regresion logistica  
     * arboles  
     * redes neuronales  
  * Regresion (prediccion) (variable continua)
     * regresion lineal  
     * redes neuronales

### No supervisados:
* Clustering (descriptivo)
* Asociacion (descriptivo)
* Reduccion de dimensionalidad

## R
## Regresión Lineal
* lm(formula)

# Clase 2
* Sintaxis R
* rstudio
* R packages
* Tipos de datos
* Datasets disponibles
* data()
* data(package)
* leer/guardar datos

# Clase 3
## Algoritmos supervisados (I)
### Reduccion de dimensionalidad
* PCA (Principal Component Analysis)
  * prcomp(x)

### Clustering
* k-means
  * distancia intra cluster / distancia inter cluster
  * kmeans(x, centers)
  * SSE (Sum of squares error)  para cantidad optima de k
* Graficar cluster en 2d usando PCA
* distancias
* cluster gerarquico
* clustering con DBSCAN

# Clase 4
## Reglas de asociacion
* regla
* itemset
* item

## Medidas
### soporte
### confianza
### lift
>     library(arules)
>     apriori(trx, parameter=list(...))

### sistemas de recomendacion

# Clase 5
## Algoritmos supervisados (I)
* Clasificacion (prediccion) (variable discreta) (Predicción de una Clase)
    * Regresion logistica  
    * Naive Bayes
    * arboles  
    * redes neuronales  
* Regresion (prediccion) (variable continua) (Predicción de un Valor)
    * regresion lineal  
    * redes neuronales

### Modelos de Clasificación
* train
* test
* modelos
    * Logistic Regression
    * Naive Bayes Classifier
    * Support Vector Machines
    * Decision Trees
    * Boosted Trees
    * Random Forest
    * Neural Networks
    * Nearest Neighbor
* Medidas de Performance
    * Confusion Matrix
        * recall
        * precision
        * accuracy

### Regresion logistica
>     glm(formula, xtrain, family)
>     predic(modelo, newdata, type)

# Clase 6
## Algoritmos supervisados (II)
### Naive Bayes
* naiveBayes(formula, data)

## Arboles de decisión
>     rpart(formula, data = xtrain)
>     predict(modelo, xtest, type)

* matriz de confusión

## Random forest
* train - bagging - voto mayoritario

>     randomForest(formula,
>     data=Train, # datos para entrenar
>     ntree=30, #cantidad de arboles
>     mtry=6, #cantidad de variables
>     replace=T) #muestras con reemplazo

### Medidas
* OOB (out of bag)
* no class.error
* yes class.error

```
Call:
 randomForest(formula = churn ~ ., data = Train, ntree = 200,      mtry = 6, replace = T)
               Type of random forest: classification
                     Number of trees: 200
No. of variables tried at each split: 6

        OOB estimate of  error rate: 5.87%
Confusion matrix:
      no yes class.error
no  1949  42  0.02109493
yes   95 248  0.27696793
```
### importancia de las variables
* varImpPlot(Modelo_rf)

# Clase 7
Graficos y Shyni

# Clase 8
## PageRank
### Algoritmos Supervisados
* Grafos
* Page Rank
* library(igraph)

## Ensamble
* promedio

### Boosting
* algoritmos
    * AdaBoost (Adaptive Boosting)
    * Gradient Tree Boosting
    * XGBoost
* library(adabag)

### Stacking

## SVM - Support Vector Machine
* svm(formula, data = xtrain, kernel)

## overfitting

# Clase 9
## Text Mining
### Preprocesamiento
* **Representación**
    * Semantic parsing
    * Bag of words
        * Corpus
        * Tokens
        * Tokenizacion
            * Stopword
            * Stemming
                * snowball steming
        * Vectorizacion
            * Flag
            * TF (time frecuency)
            * IDF (inverse document frecuency) N/log(n)
            * TF-IDF = TF * IDF
        * Estadisticos de un texto

### Sentiment Analysis
* Lexicon Bases Methods
* Modelo Predictivo

## Web scraping
library(rvest)

**web scraping + text mining in R**

**Word cloud**
