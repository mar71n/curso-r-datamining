# clase 9
# install.packages('tm'); 
# install.packages('RTextTools');
# install.packages('SnowballC'); 
# install.packages('stringi')
# limpiar Environment. Con all.names = TRUE para mostrar las que empiaesan con .
ls(all.names = TRUE)
rm(list = ls(all.names = TRUE))


library('tm'); 
library('RTextTools'); 
library('SnowballC'); 
library('stringi'); 
library(e1071);

# Funcion para limpiar texto ----------------------------------------------------------------------
clean_corpus <- function(xcorpus){
  # Se agregan palabras al stopword
  stopwords_new= c('amaz','amzn', 'amzns','amazon','amazonc', 'amazons' ,tm::stopwords('english'))
  replacePunctuation <- content_transformer(function(x) {return (gsub("[[:punct:]]"," ", x))})
  xcorpus = tm_map(xcorpus, replacePunctuation)
  xcorpus = tm_map(xcorpus, content_transformer(tm::removeNumbers))
  xcorpus = tm_map(xcorpus,tm::stripWhitespace)
  xcorpus = tm_map(xcorpus,content_transformer(base::tolower))
  xcorpus = tm_map(xcorpus, PlainTextDocument)
  xcorpus = tm_map(xcorpus, stemDocument, language = "porter")
  xcorpus = tm_map(xcorpus,tm::removeWords,stopwords_new)
  return(xcorpus)
}
# Load Data -----------------------------------------------------------------------
df0 <- read.csv("amz3.csv", sep=",")
df <- df0[1:10000,]

# Muestra para Train Test -------------------------------------------------------------
n <- nrow(df)
muestra <- sample(x = n, size = n * .3)
df_Train <- df[-muestra,]
df_Test <- df[muestra,]
# Matriz Train -----------------------------------------------------------------------
train_v_corpus <- tm::VCorpus(VectorSource(df_Train$texto)) # Limpia Corpus
train_v_corpus <- clean_corpus(train_v_corpus) # Crea DocumentTermMatrix
train_dtm <- DocumentTermMatrix(train_v_corpus,
                                control = list(weight = weightTfIdf,
                                               bounds = list(global = c(2,Inf)),
                                               wordLengths=c(2,8)))
train_mtx <- as.matrix(train_dtm) # Crea matriz de train
ytrain <- as.factor(df_Train$clase) # Crea clase para train
# Matriz Test -----------------------------------------------------------------------
test_v_corpus <- tm::VCorpus(VectorSource(df_Test$texto))
test_v_corpus <- clean_corpus(test_v_corpus)
test_dtm <- DocumentTermMatrix(test_v_corpus,
                               control = list(dictionary = Terms(train_dtm),
                                              weight = weightTfIdf ))
test_mtx <- as.matrix(test_dtm)
ytest <- as.factor(df_Test$clase) # Clase para test

# Modelo -----------
modelo <- svm(x=train_mtx, y=ytrain, kernel = 'linear', scale = FALSE)
# Prediccion
prediccion <- predict(modelo,test_mtx)
(mc <- table(ytest,prediccion))
sum(diag(mc))/sum(mc)


## web scraping
# Cargar paquete rvest. Instalarlo antes si es necesario.
library('rvest') # previamente deben instalarlo
# Indicar la URL de la cual se extraerá su HTML para hacer scraping.
url90 <-'https://www.imdb.com/search/title?count=250&release_date=1990,1999&title_type=feature'
webpage90s <- read_html(url90)
# Usar el modo Developer del Chrome para inspeccionar el CSS de los elementos
rank_data_html <- html_nodes(webpage90s,'.lister-item-index')
rank_data <- as.numeric(html_text(rank_data_html))
# Extraer titulos de las películas
title_data_html <- html_nodes(webpage90s,'.lister-item-header a')
title_data <- html_text(title_data_html)
# Extraer la descripción y quitarles Enters y espacios
description_data_html <- html_nodes(webpage90s,'.ratings-bar+ .text-muted')
description_data <- gsub("\n ","",html_text(description_data_html))

# A la duración le quitamos 'min' y lo convertimos a número
runtime_data_html <- html_nodes(webpage90s,'.text-muted .runtime')
runtime_data <- as.numeric(gsub(" min","",html_text(runtime_data_html))) # en minutos
# Obtenermos sólo el 1er género de cada pelicula
genres_data_html <- html_nodes(webpage90s,'.genre')
genres_data <- gsub(" ", "", gsub("\n","",html_text(genres_data_html)))
genre_data <- gsub(",.*","",genres_data)
genre_data <- as.factor(genre_data)
rating_data_html <- html_nodes(webpage90s,'.ratings-imdb-rating strong')
rating_data <- as.numeric(html_text(rating_data_html))
votes_data_html <- html_nodes(webpage90s,'.sort-num_votes-visible span:nth-child(2)')
votes_data <- as.numeric(gsub(",","",html_text(votes_data_html)))
directors_data_html <- html_nodes(webpage90s,'.text-muted+ p a:nth-child(1)')
directors_data <- as.factor(html_text(directors_data_html))

actors_data_html <- html_nodes(webpage90s,'.lister-item-content .ghost+ a')
actors_data <- as.factor(html_text(actors_data_html))
gross_data_html <- html_nodes(webpage90s,'.ghost~ .text-muted+ span')
gross_data <- substring(gsub("M","",html_text(gross_data_html)), 2, 6)
movies_df <- data.frame(Rank = rank_data,
                        Title = title_data,
                        Description = description_data,
                        Runtime = runtime_data,
                        Genre = genre_data,
                        Rating = rating_data,
                        Votes = votes_data,
                        Gross_Earning_in_Mil = gross_data,
                        Director = directors_data,
                        Actor = actors_data)
str(movies_df)

library('ggplot2')
ggplot(data = movies_df, mapping = aes(x = Runtime, y = Rating)) +
  geom_point(mapping = aes(size = Votes, col = Genre))
reorder_size <- function(x) {
  factor(x, levels = names(sort(table(x))))
}
ggplot(data = movies_df) +
  geom_bar(mapping = aes(x = reorder_size(Genre)))
ggplot(data = movies_df) +
  stat_summary(
    mapping = aes(x = Genre, y = Votes),
    fun.ymin = min,
    fun.ymax = max,
    fun.y = median
  )

# Generar dataset con 2000 observaciones y sólo Descripción y Género de películas
library('rvest') # previamente deben instalarlo
url90 <- 'https://www.imdb.com/search/title?count=2000&release_date=1990,1999&title_type=feature'
webpage90s <- read_html(url90)
description_data_html <- html_nodes(webpage90s,'.ratings-bar+ .text-muted')
description_data <- gsub("\n ","",html_text(description_data_html))
genres_data_html <- html_nodes(webpage90s,'.genre')
genres_data <- gsub(" ", "", gsub("\n","",html_text(genres_data_html)))
genre_data <- gsub(",.*","",genres_data)
genre_data <- as.factor(genre_data)
df <- data.frame(Description = description_data, Genre = genre_data)
# Agregar una variable para indicar si se trata de una Comedia
df$EsComedia[df$Genre == 'Comedy'] <- 'Si'
df$EsComedia[df$Genre != 'Comedy'] <- 'No'

# Luego se puede utilizar el mismo algoritmo ‘Bag of Words’ para la predicción de género Comedia
library('tm'); library('RTextTools'); library('SnowballC'); library('stringi')
clean_corpus <- function(xcorpus){
  replacePunctuation <- content_transformer(function(x) {return (gsub("[[:punct:]]"," ", x))})
  xcorpus = tm_map(xcorpus, replacePunctuation)
  xcorpus = tm_map(xcorpus, content_transformer(tm::removeNumbers))
  xcorpus = tm_map(xcorpus,tm::stripWhitespace)
  xcorpus = tm_map(xcorpus,content_transformer(base::tolower))
  xcorpus = tm_map(xcorpus, PlainTextDocument)
  xcorpus = tm_map(xcorpus, stemDocument, language = "porter")
  return(xcorpus)
}
n <- nrow(df)
muestra <- sample(x = n, size = n * .3)
df_Train <- df[-muestra,]
df_Test <- df[muestra,]
train_v_corpus <- tm::VCorpus(VectorSource(df_Train$Description)) # Limpia Corpus
train_v_corpus <- clean_corpus(train_v_corpus) # Crea DocumentTermMatrix
train_dtm <- DocumentTermMatrix(train_v_corpus,
                                control = list(weight = weightTfIdf,
                                               bounds = list(global = c(2,Inf)),
                                               wordLengths=c(4,Inf)))
train_mtx <- as.matrix(train_dtm) # Crea matriz de train
ytrain <- as.factor(df_Train$EsComedia) # Crea clase para train
test_v_corpus <- tm::VCorpus(VectorSource(df_Test$Description))
test_v_corpus <- clean_corpus(test_v_corpus)
test_dtm <- DocumentTermMatrix(test_v_corpus,
                               control = list(dictionary = Terms(train_dtm),
                                              weight = weightTfIdf ))
test_mtx <- as.matrix(test_dtm)
ytest <- as.factor(df_Test$EsComedia) # Clase para test
# Modelo
modelo <- svm(x=train_mtx, y=ytrain, kernel = 'linear', scale = FALSE)
# Predicción
prediccion <- predict(modelo,test_mtx)
(mc <- table(ytest,prediccion))
sum(diag(mc))/sum(mc)

## word clow
# install.packages("SnowballC");
# install.packages("wordcloud");
library("tm")
library("SnowballC")
library("wordcloud")
library("RColorBrewer")
# FUNCION
clean_corpus <- function(xcorpus){
  stopwords_new= c('no','si', tm::stopwords('spanish'))
  replacePunctuation <- content_transformer(function(x) {return (gsub("[[:punct:]]"," ", x))})
  xcorpus = tm_map(xcorpus, replacePunctuation)
  xcorpus = tm_map(xcorpus, content_transformer(tm::removeNumbers))
  xcorpus = tm_map(xcorpus,tm::stripWhitespace)
  xcorpus = tm_map(xcorpus,content_transformer(base::tolower))
  #xcorpus = tm_map(xcorpus, stemDocument, language = "spanish")
  xcorpus = tm_map(xcorpus,tm::removeWords,stopwords_new)
  return(xcorpus)
}
# D1
text <- readLines("C:/discursos/21mayo2006.txt",encoding="UTF-8")
docs <- Corpus(VectorSource(text))
docs <- clean_corpus(docs)
dtm <- TermDocumentMatrix(docs)
m <- as.matrix(dtm)
v <- sort(rowSums(m),decreasing=TRUE)
d1 <- data.frame(word = names(v),freq=v)
# D2
text <- readLines("C:/discursos/21mayo2014.txt",encoding="UTF-8")
docs <- Corpus(VectorSource(text))
docs <- clean_corpus(docs)
dtm <- TermDocumentMatrix(docs)
m <- as.matrix(dtm)
v <- sort(rowSums(m),decreasing=TRUE)
d2 <- data.frame(word = names(v),freq=v)
