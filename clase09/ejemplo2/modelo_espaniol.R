# install.packages('tm'); install.packages('RTextTools'); 
# install.packages('SnowballC'); install.packages('stringi')
rm(list = ls(all.names = TRUE))

library('tm'); library('RTextTools'); library('SnowballC'); library('stringi')
library(e1071)
# Funcion para limpiar texto ----------------------------------------------------------------------
clean_corpus <- function(xcorpus){
  # Se agregan palabras al stopword
  stopwords_new= c('la', 'le', tm::stopwords('spanish'))
  replacePunctuation <- content_transformer(function(x) {return (gsub("[[:punct:]]"," ", x))})
  xcorpus = tm_map(xcorpus, replacePunctuation) 
  xcorpus = tm_map(xcorpus, content_transformer(tm::removeNumbers))
  xcorpus = tm_map(xcorpus,tm::stripWhitespace)
  xcorpus = tm_map(xcorpus,content_transformer(base::tolower))
  xcorpus = tm_map(xcorpus, PlainTextDocument)
  xcorpus = tm_map(xcorpus, stemDocument, language = "spanish")
  xcorpus = tm_map(xcorpus,tm::removeWords,stopwords_new)
  return(xcorpus)
}

# Load Data -----------------------------------------------------------------------
df0 <- read.csv("ejemplo2/dataset.csv")
df=df0[0:5000,]
df$texto <- df$Comentario

# Train Test -----------------------------------------------------------------------
n <- nrow(df)
muestra <- sample(x = n, size = n * .3)
df_Train           <- df[-muestra,]
df_Test            <- df[muestra,]

# Matriz Train -----------------------------------------------------------------------
train_v_corpus   <- tm::VCorpus(VectorSource(df_Train$texto)) # Limpia Corpus
train_v_corpus   <- clean_corpus(train_v_corpus) # Crea DocumentTermMatrix
train_dtm        <- DocumentTermMatrix(train_v_corpus,
                                       control = list(weight = weightTfIdf, 
                                                      bounds = list(global = c(2,Inf)), 
                                                      wordLengths=c(2,8)))
train_mtx <- as.matrix(train_dtm) # Crea matriz de train
ytrain    <- as.factor(df_Train$clase)  # Crea clase para train
dim(train_mtx)

# Matriz Test  -----------------------------------------------------------------------
test_v_corpus   <- tm::VCorpus(VectorSource(df_Test$texto))
test_v_corpus   <- clean_corpus(test_v_corpus) 
test_dtm        <- DocumentTermMatrix(test_v_corpus,
                                      control = list(dictionary = Terms(train_dtm),
                                                     weight = weightTfIdf ))
test_mtx <- as.matrix(test_dtm) 
ytest    <- as.factor(df_Test$clase)  # Clase para test


# Modelo -------------------------------------------------
modelo  <- svm(x=train_mtx, y=ytrain, kernel = 'linear', scale = FALSE, probability = T)

# Prediccion -----------------------------------------------
prediccion <- predict(modelo,test_mtx)
(mc <- table(ytest,prediccion))
sum(diag(mc))/sum(mc)

# Test data ---------------------------------------
new_test <- c('que verguenza, es mentira, ladron')
new_test <- c('Felicidades por el triunfo y muchos exitos.')
new_test <- c('Felicidades al mejor equipo de los ultimos 50 años.')
new_test <- c('que verguenza, el mejor equipo de los ultimos 50 años.')
new_test <- c('fuera gato')
corpus <- VCorpus(VectorSource(new_test))
corpus <- clean_corpus(corpus)
tdm <- DocumentTermMatrix(corpus, control = list(dictionary = Terms(train_dtm)))
test <- as.matrix(tdm)

# Check accuracy on test.
predict(modelo, newdata = test, probability = T)



