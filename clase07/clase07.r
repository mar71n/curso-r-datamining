# clase 07

# Se puede instalar sólo ggplot2 aunque conviene instalar tidyverse que lo contiene
# Luego utilizar la library tidyverse o sólo ggplot2
# install.packages("tidyverse")
library(ggplot2)
ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ, y = hwy))
# Sintaxis: El ‘+’ va al final de la primera línea y no al principio de la segunda)

str(mpg)
help(mpg)
summary(mpg)

ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ, y = cyl))

ggplot(data = mpg) +
  geom_point(mapping = aes(x = class, y = drv))

ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ, y = hwy, color = class, size=cyl))

# Son opcionales y corresponden a otros parámetros como mapping. Probar combinaciones
ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ, y = hwy), color = "blue", size = 3, shape = 24,
             fill = "red")

ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ, y = hwy, color = hwy, size=hwy))

ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ, y = hwy, color = hwy < 25))

ggplot(data = mpg) +
  geom_point(mapping = aes(x = displ, y = hwy)) +
  facet_grid(drv ~ .) # Probar también con: facet_grid(. ~ cyl)
  #facet_grid(. ~ cyl)

ggplot(data = mpg) +
  geom_smooth(mapping = aes(x = displ, y = hwy))

ggplot(data = mpg) +
  geom_smooth(mapping = aes(x = displ, y = hwy, linetype = drv))

ggplot(data = mpg, mapping = aes(x = displ, y = hwy)) +
  geom_point(mapping = aes(color = class)) +
  geom_smooth(data = dplyr::filter(mpg, class == "subcompact"), se = FALSE)

ggplot(data = diamonds) +
  geom_bar(mapping = aes(x = cut, y = ..prop.., group = 1))

#install.packages("maps")
library(maps)
help(map_data)
paises <- map_data("world")
str(paises)
paises[grep("Argentina",paises$region ),]
pais <- paises[paises$region == "Argentina",]
ggplot(pais, aes(long, lat, group = group)) +
  geom_polygon(fill = "white", colour = "black") +
  geom_point(mapping = aes(x = -70, y = -40)) +
  geom_point(mapping = aes(x = -63, y = -35)) +
  geom_point(mapping = aes(x = -65, y = -30)) +
  coord_quickmap()


# CARGA LIBRERIA Y DATOS ------------------------
# install.packages("ggmap")
# install.packages("scales")
# remove.packages("ggmap")
# aparentemente ggmap:qmplot tiene una incomativilidad con
# ggplot2 v3.0.0
# Para que funcione tuve que instalar la version de desarrollo de ggmap
# install.packages("devtools")
# devtools::install_github("dkahle/ggmap")
library(ggplot2)
library(ggmap)
geodata <- data.frame(
  lat = c(-34.6555,-34.6559,-34.7085,-34.5877,
          -34.6995,-34.6037,-34.7678,-34.7133, -34.7966),
  lon = c(-58.6452,-58.6167,-58.5859,-58.532,
          -58.3921,-58.3816,-58.3792,-58.3711, -58.276))
# MAPA 1 -------------------------
qmplot(lon, lat, data = geodata,
       colour = I("red"),
       size = I(6),
       darken = .2,
       alpha = 0.2,
       source = 'google'
       )

# CARGA LIBRERIA Y DATOS ------------------------
library(ggplot2); library(ggmap)
geodata <- data.frame(
  lat = c(-34.6555,-34.6559,-34.7085,-34.5877,
          -34.6995,-34.6037,-34.7678,-34.7133, -34.7966),
  lon = c(-58.6452,-58.6167,-58.5859,-58.532,
          -58.3921,-58.3816,-58.3792,-58.3711, -58.276))
# MAPA 2 -------------------------
#ciudad <- get_map(location = "Buenos Aires, CABA, Argentina", zoom=10)
ciudad <- get_map(c(-58.53861607,-34.601236), zoom=10)
save(ciudad, file="ciudad_gcba.rda")
rm(ciudad)
load(file="ciudad_gcba.rda", verbose = TRUE)
p2 <- ggmap(ciudad)
p2 + geom_point(data = geodata,
               aes(x = lon, y = lat),
               color = "red",
               size = 11,
               alpha = 0.5)

str(ciudad)
# salvo los atributos
abb <- attr(ciudad, "bb")
asource <- attr(ciudad, "source")
amaptype <- attr(ciudad, "maptype")
azoom <- attr(ciudad, "zoom")
cciudad <- class(ciudad)
# toco el mapa
ciudad[1:1280] <- "#FF0000"  # pinta la primer columna de rojo
ciudad[ seq(1,1638400, 1281)] <- "#FF0000"  # traza una diagonal roja
ciudad[ as.vector(sapply(seq(0,120), function(i){seq(1200+(i*1280),1279+(i*1280))}))] <- "#FF0000" # tacho la firma

# una vez que se toco el mapa de bits, hay que restaurar los atributos del objeto
dim(ciudad)
class(ciudad) <- cciudad
attr(ciudad, "bb") <- abb
attr(ciudad, "source") <- asource
attr(ciudad, "maptype") <- amaptype
attr(ciudad, "zoom") <- azoom
ggmap(ciudad)

# CARGA LIBRERIA Y DATOS ------------------------
library(ggplot2); library(ggmap)
geodata <- data.frame(
  lat = c(-34.5,-34.5,-34.6555,-34.6555,-34.6559,-34.7085,-34.5877,
          -34.6995,-34.6037,-34.7678,-34.7133, -34.7966),
  lon = c(-58.6452,-58.6452,-58.6452,-58.6452,-58.6167,-58.5859,-58.532,
          -58.3921,-58.3816,-58.3792,-58.3711, -58.276))
# MAPA 3 ------------------------
ciudad <- get_map("Buenos Aires, Argentina", zoom=10)
p <- ggmap(ciudad)
p <- p+stat_density2d(aes(x = lon, y = lat, fill=..level..),
                      data=geodata,geom="polygon", alpha=0.2, size=101)
p + scale_fill_gradient(low = "yellow", high = "red")

library(shiny)
runExample("01_hello")
