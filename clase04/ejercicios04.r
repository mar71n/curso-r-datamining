# Ejercicios de práctica - Clase 4
# 1) Para cada una de las siguientes preguntas, proporcionar un ejemplo de regla de asociación
# aplicable al dominio de canasta de mercado, que satisfaga las siguientes condiciones. Explicar
# además si tales reglas son subjetivamente interesantes.
# N = cantidad de transacciones 
# support = freq(X,Y) / N
# confidence = freq(X,Y) / freq(X)
# support = confidence <=> freq(X) = N
# support({}) = N
# a) Una reglas que tiene alto soporte y alta confianza.
# b) Una regla que tiene un soporte razonablemente alto pero baja confianza.
# c) Una regla que tiene bajo soporte y baja confianza.
# d) Una regla que tiene bajo soporte y alta confianza.
trx_csv <- read.csv("groceries_4.csv")
library(arulesViz);library(arules)
trx <- read.transactions("groceries_4.csv",
                         format = "single",
                         sep = ",",
                         cols = c("Id_Factura", "Item")
)
str(trx)
trx_df <- as(trx, "data.frame")
N <- nrow(trx_df) # 9835
nrow(trx_df[grep("sparkling wine",trx_df$items),]) # 51 
# 51/9835 =  0.005185562
# > inspect(head(reglas,25))
# lhs    rhs                        support     confidence  lift count
# [1]  {}  => {sparkling wine}           0.005185562 0.005185562 1     51  

reglas <- apriori(trx, parameter=list(support=0.001, confidence = 0.005))
print(reglas) # 14945
inspect(head(reglas,25))
reglas_df <- as(reglas,"data.frame")
summary(reglas_df$confidence)
summary(reglas_df$support)
hist(reglas_df$confidence)
hist(reglas_df$support, breaks = 100)
reglas_df[which(reglas_df$support > 0.07 & reglas_df$confidence > 0.2),]


## 2) Considere el data set mostrado en la tabla siguiente.
# ID DE CLIENTE ID DE TRANSACCION ITEMS COMPRADOS
# 1 0001 {a, d, e}
# 1 0024 {a, b, c, e}
# 2 0012 {a, b, d, e}
# 2 0031 {a, c, d, e}
# 3 0015 {b, c, e}
# 3 0022 {b, d, e}
# 4 0029 {c, d}
# 4 0040 {a, b, c}
# 5 0033 {a, d, e}
# 5 0038 {a, b, e}
trx_ej02 <- read.transactions("super_ej02.csv",
                         format = "single",
                         sep = ",",
                         cols = c("ID_TRANSACCION", "ITEMS")
)
trx_ej02_df <- as(trx_ej02,"data.frame")
show(trx_ej02)
transactionInfo(trx_ej02)
labels(trx_ej02)
reglas_ej02 <- apriori(trx_ej02, parameter=list(support=0.0001, confidence = 0.000005))
print(reglas_ej02)
reglas_ej02@lhs@itemInfo
reglas_ej02@rhs@itemsetInfo
reglas_ej02_df <- as(reglas_ej02,"data.frame")
reglas_ej02_eclat <- eclat(trx_ej02)
reglas_ej02_eclat_df <- as(reglas_ej02_eclat, "data.frame")

## a) Computar el soporte para los itemsets {e}, {b, d}, y {b, d, e}.
length(grep("e",labels(trx_ej02)))
# s({e}) = 8 / 10 = 0.8
length(intersect(grep("b",labels(trx_ej02)),grep("d",labels(trx_ej02))))
# s({b,d}) = 2 / 10 = 0.2
X1 <- grep("b",labels(trx_ej02))
X2 <- grep("d",labels(trx_ej02))
X3 <- grep("e",labels(trx_ej02))
length(intersect(intersect(X1,X2), X3))
# s({b, d, e}) = 2 / 10 = 0.2

## b) Usar los resultados de a) para computar la confianza para las reglas de asociación
# b d → e y e → b d
# c(b d → e) = s({b, d, e}) / s({b, d}) = 0.2 / 0.2 = 1
# c(e → b d) = s({e, b, d}) / s({e}) = 0.2 / 0.8 = 0.25
# Note: Apriori only creates rules with one item in the RHS (Consequent)!
# ¿la confianza es una medida simétrica?
# Rta: NO

## c) Repetir la parte a) pero tratando cada ID de cliente como una canasta. Cada ítem
# debería ser tratado como una variable binaria.
trx_ej02_cli <- read.transactions("super_ej02.csv",
                              format = "single",
                              sep = ",",
                              cols = c("ID_CLIENTE", "ITEMS")
)
trx_ej02_cli_df <- as(trx_ej02_cli, "data.frame")
labels(trx_ej02_cli)
reglas_ej02_cli <- apriori(trx_ej02_cli, parameter=list(support=0.0001, confidence = 0.000005))
reglas_ej02_cli_df <- as(reglas_ej02_cli, "data.frame")
# 1 0001 {a, d, e} U {a, b, c, e} = {a, b, c, d, e}
# 1 0024 
# 2 0012 {a, b, d, e} U {a, c, d, e} = {a, b, c, d, e}
# union(c("a", "b", "d", "e") ,c("a", "c", "d", "e"))
# 2 0031 
# 3 0015 {b, c, e} U {b, d, e} = {b, c, d, e}
# 3 0022 
# 4 0029 {c, d} U {a, b, c} = {a, b, c, d}
# 4 0040 
# 5 0033 {a, d, e} U {a, b, e} = {a, b, d, e}
# 5 0038 
length(grep("e",labels(trx_ej02_cli)))
# s({e}) = 4 / 5 = 0.8
length(intersect(grep("b",labels(trx_ej02_cli)),grep("d",labels(trx_ej02_cli))))
# s({b,d}) = 5 / 5 = 1
X1 <- grep("b",labels(trx_ej02_cli))
X2 <- grep("d",labels(trx_ej02_cli))
X3 <- grep("e",labels(trx_ej02_cli))
length(intersect(intersect(X1,X2), X3))
# s({b, d, e}) = 4 / 5 = 0.8
reglas_ej02_cli <- apriori(trx_ej02_cli)
reglas_ej02_cli_df <- as(reglas_ej02_cli, "data.frame")

## d) Usar los resultados en c) para computar la confianza para las reglas
# b d → e y e → b d
# c(b d → e) = s({b, d, e}) / s({b, d}) = 0.8 / 1 = 0.8
# c(e → b d) = s({e, b, d}) / s({e}) = 0.8 / 0.8 = 1

## e) Suponer que s1 y c1 son el soporte y la confianza de una regla r cuando se trata cada ID
# de transacción como una canasta. Sean también s2 y c2 el soporte y confianza de r
# cuando se trata a cada ID de cliente como una canasta. Discutir si hay alguna relación
# entre s1 y s2 o c1 y c2.
# s1 <= s2
# 