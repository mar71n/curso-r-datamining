install.packages("arules")
install.packages("arulesViz")
library(arules)
library(arulesViz)

# CARGA LIBRERÍA Y DATOS
library(arulesViz);library(arules)
#url.data <- "https://www.dropbox.com/s/tub6vmcfb319uo3/groceries_4.csv?dl=1"
trx <- read.transactions("groceries_4.csv",
                           format = "single",
                           sep = ",",
                           cols = c("Id_Factura", "Item")
)
summary(trx)
str(trx)
trx(1)
?transactions
trx_matrix <- as(trx,"matrix")
trx_df <- as(trx,"data.frame")
trx_list <- as(trx,"list")
head(trx_df)
nrow(trx_df)
nrow(trx_df[trx_df$items=="{whole milk}",])
nrow(trx_df[grep("whole milk",trx_df$items),]) # 2365
# 2365 / 9835 = 0.2404677


# CREA REGLAS
reglas <- apriori(trx, parameter=list(support=0.01, confidence = 0.05))
print(reglas) # Cantidad de reglas creadas
reglas_df <- as(reglas,"data.frame")
reglas_df[reglas_df$rules == "{} => {whole milk}",]
#rules                  support  confidence lift count
#27 {} => {whole milk} 0.2404677  0.2404677    1  2365
# PRINT
# imprime las 3 reglas con mayor confianza
reglas <- sort(reglas, by = "confidence", decreasing = TRUE) # ordena regla
inspect(head(reglas,3))
# ANÁLISIS DE FRECUENCIA DE ÍTEMS
# Crea data.frame con frecuencia porcentual de cada producto
FreqProd <- data.frame(Producto=names(itemFrequency(trx)),
                         Frecuencia=itemFrequency(trx), row.names=NULL)
FreqProd <- FreqProd[order(FreqProd$Frecuencia, decreasing = T),] # Order de DF
head(FreqProd, 10) # print de diez productos más frecuentes

# PLOT
# Gráfico de los 20 productos más frecuente
itemFrequencyPlot(trx, topN = 20, type = "absolute")
# Gráfico de 20 reglas con mayor confianza
reglas <- sort(reglas, by= "confidence", decreasing=TRUE) # ordena
reglas
plot(head(reglas,50), method="graph")
# Gráfico de matriz de 100 reglas con mayor confianza
plot(head(reglas,100), method = "matrix")
# gráfico de dispersión de todas las reglas
plot(reglas)
# Gráfico de burbuja de X → Y o lhs → rhs
plot(head(reglas, 80), method = "grouped")

# -------------------------------------------------------------------
datosonline <- read.csv("online_retail.csv")
str(datosonline)
head(datosonline)
sort(table(datosonline$Country), decreasing = TRUE)
trx <- read.transactions("online_retail.csv",
                         format = "single",
                         sep = ",",
                         cols = c("CustomerID", "Description")
)
summary(trx)
reglas <- apriori(trx, parameter=list(support=0.01, confidence = 0.05))
print(reglas) # Cantidad de reglas creadas
# PRINT
# imprime las 3 reglas con mayor confianza
reglas <- sort(reglas, by = "confidence", decreasing = TRUE) # ordena regla
inspect(head(reglas,3))
# ANÁLISIS DE FRECUENCIA DE ÍTEMS
# Crea data.frame con frecuencia porcentual de cada producto
FreqProd <- data.frame(Producto=names(itemFrequency(trx)),
                       Frecuencia=itemFrequency(trx), row.names=NULL)
FreqProd <- FreqProd[order(FreqProd$Frecuencia, decreasing = T),] # Order de DF
head(FreqProd, 10) # print de diez productos más frecuentes
