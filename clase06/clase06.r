# clase 06
0.4285*0.5*0.5*0.83*0.3333
0.5714*0.25*0.375*0.25*0.75

install.packages("e1071")
library(e1071)
# Crea dataset de Train y Test
#url.train <- "https://www.dropbox.com/s/xod536s5yjcnojg/Datos_Historicos.csv?dl=1"
#url.test <- "https://www.dropbox.com/s/caajkeugiynwlnk/Nuevos_Clientes.csv?dl=1"
url.train <- "Datos_Historicos.csv"
url.test <- "Nuevos_Clientes.csv"
Train <- read.csv(url.train)
Test <- read.csv(url.test)
# Crea modelo naive bayes
modelo_nb <- naiveBayes(Compra ~., data=Train[, -1])
print(modelo_nb)
# Prediccion de la Clase sobre Test
Prediccion <- predict(modelo_nb, Test[, -1])
print(Prediccion)
# Prediccion ratio sobre Test
Prediccion <- predict(modelo_nb, Test[, -1], type = "raw")
print(Prediccion)
apply(Prediccion,1,sum)

# Usando el Dataset “churn”, realizar:
# 1. Dividir en Train y Test
data1 <- read.csv("../clase05/churn.csv")
datos <- data1
set.seed(1)
n <- nrow(datos)
muestra <- sample(x = n, size = n * .3)
Train <- datos[-muestra,]
Test <- datos[muestra,]
# 2. Ajustar un modelo de Regresión Logística
 # Crea modelo predictivo
 Modelo_glm <- glm(churn ~., Train, family = binomial(link = "logit"))
 # Hace predicción y crea matriz de confusión
 Prediccion_glm <- predict(Modelo_glm, newdata = Test, type = "response")
 Prediccion_glm <- round(Prediccion_glm) # Umbral = 0.5. ¿Cómo alterarlo?
 Prediccion_glm <- as.numeric(Prediccion_glm >= 0.4)
 (MC1 <- table(Test[, "churn"], Prediccion_glm))
# 3. Ajustar un modelo de Naive Bayes
 # Crea modelo naive bayes
 modelo_nb <- naiveBayes(churn ~., data=Train[, -1])
 print(modelo_nb)
 # Hace predicción y crea matriz de confusión
 Prediccion_nb <- predict(modelo_nb, newdata = Test)
 Prediccion_nb <- round(Prediccion_nb) # Umbral = 0.5. ¿Cómo alterarlo?
 Prediccion_nb <- as.numeric(Prediccion >= 0.4)
 (MC <- table(Test[, "churn"],Prediccion_nb))
# 4. Calcular matriz de confusión y sus métricas para cada modelo
 (MC1 <- table(Test[, "churn"], Prediccion_glm))
 (MC <- table(Test[, "churn"],Prediccion_nb))
# 5. Identificar cuál es el mejor modelo para este dataset calculando la
# precisión de cada uno
 (precision_ <- MC1[2,2] / (MC1[2,2] + MC1[1,2]))
 (recall_ <- MC1[2,2] / (MC1[2,2] + MC1[2,1]))
 (accuracy <- (MC1[2,2] + MC1[1,1]) / sum(MC1) )
 
 (precision_ <- MC[2,2] / (MC[2,2] + MC[1,2]))
 (recall_ <- MC[2,2] / (MC[2,2] + MC[2,1]))
 (accuracy <- (MC[2,2] + MC[1,1]) / sum(MC) )
 
# -------------------------------------------------------------
# Carga Package y Set de datos
# install.packages('rpart')
# install.packages('rpart.plot')
library(rpart); library(rpart.plot)
url.data <- '../clase05/churn.csv'
data0 <- read.csv(url.data)
datos <- data0[, c(4,7,16,19,17,20)]
datos <- data0[, c(5,8,17,20,18,21)]
# Divide set en Train y Test
n <- nrow(datos)
set.seed(1)
muestra <- sample(x = n, size = n * .3)
Train <- datos[-muestra,]
Test <- datos[muestra,]
# Crea modelo
ModeloArbol <- rpart(churn ~ .,data=Train)
# Prediccion en TEST
Prediccion_tree <- predict(ModeloArbol, Test, type="class")
(MC_tree <- table(Test[, "churn"],Prediccion_tree) )
# Grafico
rpart.plot(ModeloArbol, type=4, extra=101,cex = .7,
          box.col=c("gray99", "gray88")[ModeloArbol$frame$yval])
(precision_ <- MC_tree[2,2] / (MC_tree[2,2] + MC_tree[1,2]))

data0$canario <- 

# -----------------------------------------------------------------
# Carga package y datos
# install.packages('randomForest')
library(randomForest)
#url.data <- 'https://www.dropbox.com/s/3u8fq8tl67iz88q/churn.csv?dl=1'
data0 <- read.csv("../clase05/churn.csv")
datos <- data0
# Divide set en Train y Test
n <- nrow(datos)
set.seed(1)
muestra <- sample(x = n, size = n * .3)
Train <- datos[-muestra,]
Test <- datos[muestra,]
# Crea modelo predictivo
Modelo_rf <-randomForest(churn ~ .,
                         data=Train, # datos para entrenar
                         ntree=200, # cantidad de arboles
                         mtry=6, # cantidad de variables
                         replace=T) # muestras con reemplazo
# Crea predicción y Matriz de confusión
Prediccion_rf <- predict (Modelo_rf , Test[,-21]);
# Matriz de Confusión
(MC_rf<- table(Test[, "churn"],Prediccion_rf))
(precision_ <- MC_rf[2,2] / (MC_rf[2,2] + MC_rf[1,2]))
plot(Modelo_rf)
varImpPlot(Modelo_rf)
print(Modelo_rf)