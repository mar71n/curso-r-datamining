## TP
# Stacking

rm(list = ls(all.names = TRUE))

train.url <- "adult_train.csv"
train0 <- read.csv(train.url)
test.url <- "adult_test.csv"
test0 <- read.csv(test.url)

set.seed(1)
nt1 <- sample(nrow(train0), 16280)

# divido train en dos para sendos niveles
train1 <- train0[nt1,]
train2 <- train0[-nt1,]

#

## Variables agregadas
generar <- function(x){
  capital_neto <<- x[["capital.gain"]] - x[["capital.loss"]]
  afortunado <<- x[["age"]] > 30 & gsub(" ","",x[["relationship"]]) == "Own-child"
  tiempo_completo <<- x[["hours.per.week"]] >= 40
  macho_alfa <<- gsub(" ","",x[["sex"]]) == "Male" &
    gsub(" ","",x[["race"]]) == "White" &
    gsub(" ","",x[["native.country"]]) == "United-States" &
    x[["hours.per.week"]] >= 40 &
    x[["education.num"]] >= 13
  educsup <- c("Doctorate", "Prof-School", "Masters", "Bachelors")
  universitario <<- gsub(" ", "",x[["education"]]) %in% educsup
  posgra <- c("Doctorate", "Prof-School", "Masters")
  posgrado <<- gsub(" ", "",x[["education"]]) %in% posgra
  prof_pri <<- gsub(" ","",x[["occupation"]]) == "Prof-specialty" & 
    gsub(" ","",x[["workclass"]]) == "Private" & 
    x[["age"]] < 40
  vsoles <- c("Divorced", "Never-married", "Separated")
  soles <<- gsub(" ","",x[["marital.status"]]) %in% vsoles & x[["age"]] > 40
  campolab <<- gsub(".*gov","Estatal",x$workclass)
  campolab <<-gsub(".*Self.*","Self",campolab)
  campolab <<-gsub(".*Priv.*","Private",campolab)
  campolab <<- as.factor(campolab)
}

metricas_a <- function(xtest, pred){
  print(MC <- table(test=xtest[, "class"],pred))
  precision <- MC[2,2] / (MC[2,2] + MC[1,2])
  recall <- MC[2,2] / (MC[2,2] + MC[2,1])
  c("precision" = precision, "recall" = recall)
}

# agrego variables
# a train1 train2 y test
agregadas <- c("capital_neto", "afortunado", "tiempo_completo", "macho_alfa", "universitario",
              "posgrado", "prof_pri", "soles", "campolab")
generar(train1)
train1 <- cbind(train1, capital_neto, afortunado, tiempo_completo, macho_alfa, universitario,
                posgrado, prof_pri, soles, campolab)
generar(train2)
train2 <- cbind(train2, capital_neto, afortunado, tiempo_completo, macho_alfa, universitario,
                posgrado, prof_pri, soles, campolab)
generar(test0)
test0 <- cbind(test0, capital_neto, afortunado, tiempo_completo, macho_alfa, universitario,
                posgrado, prof_pri, soles, campolab)

# ejemplo original
library(randomForest)
features <- c("class","capital.gain", "age", "relationship", "marital.status","occupation")
train <- train1[,features]
test <- test0 #[,features]
set.seed(1)
Modelo_rf0 <- randomForest(class ~ ., train, ntree = 100)
Prediccion_rf0 <- predict(Modelo_rf0, test)
metricas1 <- metricas_a(test, Prediccion_rf0)

# agrego variables propuestas
features <- c("class","capital.gain", "age", "relationship", "marital.status","occupation", "capital_neto", "afortunado", "tiempo_completo", "macho_alfa", "universitario", "posgrado")
train <- train1[,features]
test <- test0 #[,features]
set.seed(1)
Modelo_rf1 <- randomForest(class ~ ., train, ntree = 100)
Prediccion_rf1 <- predict(Modelo_rf1, test)
metricas2 <- metricas_a(test, Prediccion_rf1)

# agrego variables
features <- c("class", "workclass", "capital.gain", "age", "sex", "relationship", 
              "marital.status","occupation", "capital_neto", "afortunado", "tiempo_completo", 
              "macho_alfa", "universitario", "posgrado", "prof_pri",  "soles", "campolab")
train <- train1[,features]
test <- test0 #[,features]
set.seed(1)
Modelo_rf2 <- randomForest(class ~ ., train, ntree = 200, mtry = 3, replace = TRUE, type="class")
#Modelo_rf2 <- randomForest(class ~ ., train, ntree = 100, mtry = 4, replace = TRUE, cutoff = c(0.25,0.75))
Prediccion_rf2 <- predict(Modelo_rf2, test)
metricas3 <- metricas_a(test, Prediccion_rf2)

# a esta altura tengo train y test con las variables propuestas y las que agregue

# regresion logistica (clase5)
set.seed(1)
Modelo_glm <- glm(class ~ ., train, family = binomial(link = "logit"))
# Hace predicción y crea matriz de confusión
Prediccion_glm <- predict(Modelo_glm, newdata = test, type = "response")
#Prediccion_glm <- round(Prediccion_glm) # Umbral = 0.5
Prediccion_glm <- Prediccion_glm > 0.56 # Umbral
#(MC <- table(test[, "class"],Prediccion))
metricas4 <- metricas_a(test, Prediccion_glm)

# Crea modelo naive bayes (clase6)
library(e1071)
set.seed(1)
Modelo_nb <- naiveBayes(class ~., data=train)
Prediccion_nb <- predict(Modelo_nb, newdata = test)
metricas5 <- metricas_a(test, Prediccion_nb)

# modelo boosting
library(adabag)
set.seed(1)
modelo_ada <- boosting(class ~ ., data=train, mfinal=3)
Prediccion_ada <- predict(modelo_ada, test)$class
#(MC <- table(test[, "class"],Prediccion) )
metricas6 <- metricas_a(test, Prediccion_ada)

## Ensamblo x rl

# aplico los modelos a train2
train <- train2
#features <- c("class","capital.gain", "age", "relationship", "marital.status","occupation", 
#              "capital_neto", "afortunado", "tiempo_completo", "macho_alfa", "universitario", 
#              "posgrado", "workclass", "sex")
#train <- train[,features]
Prediccion_rf0_ <- predict(Modelo_rf0, train)
Prediccion_rf1_ <- predict(Modelo_rf1, train)
Prediccion_rf2_ <- predict(Modelo_rf2, train)
Prediccion_ada_ <- predict(modelo_ada, train)$class
Prediccion_glm_ <- predict(Modelo_glm, newdata = train, type = "response")
Prediccion_glm_ <- Prediccion_glm_ > 0.50 # Umbral
Prediccion_glm_ <- ifelse(Prediccion_glm,">50K","<=50K")
Prediccion_nb_ <- predict(Modelo_nb, newdata = train)

# creo el train para la ultima reg log
train_RL <- cbind(train, Prediccion_rf0_, Prediccion_rf1_, Prediccion_rf2_, Prediccion_ada_,
                  Prediccion_glm_, Prediccion_nb_)
features <- c("class", "Prediccion_rf0_", "Prediccion_rf1_", "Prediccion_rf2_", "Prediccion_ada_",
              "Prediccion_glm_", "Prediccion_nb_")
train_RL <- train_RL[, features]
set.seed(1)
Modelo_glm_FIN <- glm(class ~ ., train_RL, family = binomial(link = "logit"))
# Hace predicción y crea matriz de confusión
Prediccion_glm_FIN <- predict(Modelo_glm_FIN, newdata = test, type = "response")
#Prediccion_glm <- round(Prediccion_glm) # Umbral = 0.5
Prediccion_glm_FIN <- Prediccion_glm_FIN > 0.56 # Umbral
#(MC <- table(test[, "class"],Prediccion))
metricas_FIN <- metricas_a(test, Prediccion_glm)
#precision    recall 
#0.7659328 0.5156006 

print(metricas1)
print(metricas2)
print(metricas3)
print(metricas4)
print(metricas5)
print(metricas6)
print(metricas_FIN)
