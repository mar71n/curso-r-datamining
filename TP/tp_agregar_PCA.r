# TP
# agrgar variables PCA
# limpiar Environment. Con all.names = TRUE para mostrar las que empiaesan con .
ls(all.names = TRUE)
rm(list = ls(all.names = TRUE))

set.seed(1)

# train.url <- "adult_train.csv"
# train0 <- read.csv(train.url)
# numericas <- c("age", "fnlwgt", "education.num", "capital.gain", "capital.loss", "hours.per.week")
# cor(train0[,numericas])
# train0_pca <- prcomp(train0[,numericas])
# train0_pca$sdev
# train0_pca$sdev[1]/sum(train0_pca$sdev)
# train0_pca$sdev[2]/sum(train0_pca$sdev)
# train0_pca$sdev[3]/sum(train0_pca$sdev)
# # pc1 y pc2 0.931049 + 0.06514524 = 0.9961942
# # Agrego pc1 y pc2

# Train --------------------------
train.url <- "adult_train.csv"
features <- c("class","capital.gain", "age", "relationship", "marital.status","occupation")
train0 <- read.csv(train.url)
train <- train0[,features]
#train$class.sn <- train$class == " >50K"
numericas <- c("age", "fnlwgt", "education.num", "capital.gain", "capital.loss", "hours.per.week")
train_pca <- prcomp(train0[,numericas])
train <- cbind(train, pc1=train_pca$x[,"PC1"], pc2=train_pca$x[,"PC2"])
#train <- train[sample(nrow(train),nrow(train) * 0.1 ) , ] # Entreno solo con el 10% 
# Test --------------------------
test.url <- "adult_test.csv"
test0 <- read.csv(test.url)
test <- test0[,features]
#test$class.sn <- test0$class == " >50K."
numericas <- c("age", "fnlwgt", "education.num", "capital.gain", "capital.loss", "hours.per.week")
test_pca <- prcomp(test0[,numericas])
test <- cbind(test, pc1=test_pca$x[,"PC1"], pc2=test_pca$x[,"PC2"])
# Modelo --------------------------
Modelo_rf <- randomForest(class ~ ., train, ntree = 100)
# Prediccion --------------------------
Prediccion <- predict(Modelo_rf, test)
(MC <- table(test[, "class"],Prediccion))
# Metricas -----------------------------
#print("precision")
precision <- MC[2,2] / (MC[2,2] + MC[1,2])
#print("recall")
recall <- MC[2,2] / (MC[2,2] + MC[2,1])
c("precision" = precision, "recall" = recall)
print(Modelo_rf)
