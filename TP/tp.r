# TP
# Librerias --------------------------
set.seed(1)
library(randomForest)

## Ejemplo:
# Train --------------------------
train.url <- "adult_train.csv"
features <- c("class","capital.gain", "age", "relationship", "marital.status","occupation")
train0 <- read.csv(train.url)
train <- train0[,features]
# Test --------------------------
test.url <- "adult_test.csv"
test0 <- read.csv(test.url)
test <- test0[,features]
# Modelo --------------------------
Modelo_rf <- randomForest(class ~ ., train, ntree = 10)
# Prediccion --------------------------
Prediccion <- predict(Modelo_rf, test)
(MC <- table(test[, "class"],Prediccion))
# Metricas -----------------------------
#print("precision")
precision <- MC[2,2] / (MC[2,2] + MC[1,2])
#print("recall")
recall <- MC[2,2] / (MC[2,2] + MC[2,1])
c("precision" = precision, "recall" = recall)
print(Modelo_rf)
Modelo_rf$importance
Modelo_rf$confusion
Modelo_rf$err.rate
varImpPlot(Modelo_rf)
