# TP

rm(list = ls(all.names = TRUE))

train.url <- "adult_train.csv"
train0 <- read.csv(train.url)
test.url <- "adult_test.csv"
test0 <- read.csv(test.url)

## Variables agregadas
generar <- function(x){
  capital_neto <<- x[["capital.gain"]] - x[["capital.loss"]]
  afortunado <<- x[["age"]] > 30 & x[["relationship"]] == "own-child"
  tiempo_completo <<- x[["hours.per.week"]] >= 40
  macho_alfa <<- x[["sex"]] == "Male" &
    x[["race"]] == "White" &
    x[["native.country"]] == "United-States" &
    x[["hours.per.week"]] >= 40 &
    x[["education.num"]] >= 13
  educsup = c("Doctorate", "Prof-School", "Masters", "Bachelors")
  universitario <<- gsub(" ", "",x[["education"]]) %in% educsup
  posgra = c("Doctorate", "Prof-School", "Masters")
  posgrado <<- gsub(" ", "",x[["education"]]) %in% posgra
}

generar2 <- function(x){
  prof_pri <<- gsub(" ","",x[["occupation"]]) == "Prof-specialty" & 
    gsub(" ","",x[["workclass"]]) == "Private" & 
    x[["age"]] < 40
  vsoles <- c("Divorced", "Never-married", "Separated")
  soles <<- gsub(" ","",x[["marital.status"]]) %in% vsoles & x[["age"]] > 40
  campolab <<- gsub(".*gov","Estatal",x$workclass)
  campolab <<-gsub(".*Self.*","Self",campolab)
  campolab <<-gsub(".*Priv.*","Private",campolab)
  campolab <<- as.factor(campolab)
}

metricas_a <- function(xtest, pred){
  print(MC <- table(test=xtest[, "class"],pred))
  precision <- MC[2,2] / (MC[2,2] + MC[1,2])
  recall <- MC[2,2] / (MC[2,2] + MC[2,1])
  c("precision" = precision, "recall" = recall)
}

# ejemplo original
library(randomForest)
features <- c("class","capital.gain", "age", "relationship", "marital.status","occupation")
train <- train0[,features]
test <- test0[,features]
set.seed(1)
Modelo_rf <- randomForest(class ~ ., train, ntree = 100)
Prediccion_rf0 <- predict(Modelo_rf, test)
metricas1 <- metricas_a(test, Prediccion_rf0)

# agrego variables propuestas
train <- train0
test <- test0
generar(train)
train <- cbind(train0, capital_neto, afortunado, tiempo_completo, macho_alfa, universitario, posgrado)
generar(test)
test <- cbind(test0, capital_neto, afortunado, tiempo_completo, macho_alfa, universitario, posgrado)
features <- c("class","capital.gain", "age", "relationship", "marital.status","occupation", "capital_neto", "afortunado", "tiempo_completo", "macho_alfa", "universitario", "posgrado")
train <- train[,features]
test <- test[,features]
set.seed(1)
Modelo_rf <- randomForest(class ~ ., train, ntree = 100)
Prediccion_rf1 <- predict(Modelo_rf, test)
metricas2 <- metricas_a(test, Prediccion_rf1)

# agrego variables
train <- train0
test <- test0
generar(train)
train <- cbind(train0, capital_neto, afortunado, tiempo_completo, macho_alfa, universitario, posgrado)
generar(test)
test <- cbind(test0, capital_neto, afortunado, tiempo_completo, macho_alfa, universitario, posgrado)
features <- c("class", "workclass", "capital.gain", "age", "sex", "relationship", "marital.status","occupation", "capital_neto", "afortunado", "tiempo_completo", "macho_alfa", "universitario", "posgrado")
train <- train[,features]
test <- test[,features]
generar2(test)
test <- cbind(test, prof_pri,  soles, campolab)
generar2(train)
train <- cbind(train, prof_pri, soles, campolab)
set.seed(1)
Modelo_rf <- randomForest(class ~ ., train, ntree = 200, mtry = 3, replace = TRUE, type="class")
#Modelo_rf <- randomForest(class ~ ., train, ntree = 100, mtry = 4, replace = TRUE, cutoff = c(0.25,0.75))
Prediccion_rf2 <- predict(Modelo_rf, test)
metricas3 <- metricas_a(test, Prediccion_rf2)

# a esta altura tengo train y test con las variables propuestas y las que agregue

# regresion logistica (clase5)
set.seed(1)
Modelo_glm <- glm(class ~ ., train, family = binomial(link = "logit"))
# Hace predicción y crea matriz de confusión
Prediccion_glm <- predict(Modelo_glm, newdata = test, type = "response")
#Prediccion_glm <- round(Prediccion_glm) # Umbral = 0.5
Prediccion_glm <- Prediccion_glm > 0.56 # Umbral
#(MC <- table(test[, "class"],Prediccion))
metricas4 <- metricas_a(test, Prediccion_glm)

# Crea modelo naive bayes (clase6)
library(e1071)
set.seed(1)
Modelo_nb <- naiveBayes(class ~., data=train)
Prediccion_nb <- predict(Modelo_nb, newdata = test)
metricas5 <- metricas_a(test, Prediccion_nb)

# modelo boosting
library(adabag)
set.seed(1)
modelo_ada <- boosting(class ~ ., data=train, mfinal=3)
Prediccion_ada <- predict(modelo_ada, test)$class
#(MC <- table(test[, "class"],Prediccion) )
metricas6 <- metricas_a(test, Prediccion_ada)

# Ensamble x voto
Prediccion_glm_ <- ifelse(Prediccion_glm,">50K","<=50K")
Prediccion_rf0_ <- gsub(" ","",Prediccion_rf0)
Prediccion_rf1_ <- gsub(" ","",Prediccion_rf1)
Prediccion_rf2_ <- gsub(" ","",Prediccion_rf2)
Prediccion_nb_ <- gsub(" ","",Prediccion_nb)
Prediccion_ada_ <- gsub(" ","",Prediccion_ada)
test_st <- cbind(test, Prediccion_rf0_, Prediccion_rf1_, Prediccion_rf2_, Prediccion_glm_, Prediccion_nb_, Prediccion_ada_ )
votos <- function(x, desempata){
  xt <- table(x)
  nxt <- names(xt)
  if(length(nxt) == 2){
    if(xt[nxt[1]] == xt[nxt[2]]){
      return(x[desempata])
    }
    if(xt[nxt[1]] > xt[nxt[2]]){
      return(nxt[1])
    } else{
      return(nxt[2])
    }
  } else{
    return(names(xt))
  }
}
test_st$pred <- apply(test_st[, c("Prediccion_rf0_","Prediccion_rf1_","Prediccion_rf2_", "Prediccion_glm_", "Prediccion_nb_", "Prediccion_ada_")],1,function(x){ votos(x,"Prediccion_rf2_")})
table(test_st[["class"]], test_st[["pred"]])
MC_ts <- table(test_st[["class"]], test_st[["pred"]])
precision <- MC_ts[2,2] / (MC_ts[2,2] + MC_ts[1,2])
recall <- MC_ts[2,2] / (MC_ts[2,2] + MC_ts[2,1])
c("precision" = precision, "recall" = recall)
#precision    recall 
#0.7828810 0.5850234