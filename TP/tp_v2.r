## TP

rm(list = ls(all.names = TRUE))

train.url <- "adult_train.csv"
train0 <- read.csv(train.url)
test.url <- "adult_test.csv"
test0 <- read.csv(test.url)

## Variables agregadas
generar <- function(x){
  # los objetos que se llamen va_... los agrego automaticamente a train y test
  #x <- train0
  va_capital_neto <<- x[["capital.gain"]] - x[["capital.loss"]]
  va_log_cap_net <<- log(va_capital_neto)
  va_log_cap_net <<- ifelse(is.integer(va_log_cap_net), va_log_cap_net, 0)
  #va_log_cap.gan <<- log(x$capital.gain)
  #va_log_cap.gan <<- ifelse(is.integer(va_log_cap.gan),va_log_cap.gan, 0)
  va_afortunado <<- x[["age"]] > 30 & gsub(" ","",x[["relationship"]]) == "Own-child"
  va_tiempo_completo <<- x[["hours.per.week"]] >= 40
  va_macho_alfa <<- gsub(" ","",x[["sex"]]) == "Male" &
    gsub(" ","",x[["race"]]) == "White" &
    gsub(" ","",x[["native.country"]]) == "United-States" &
    x[["hours.per.week"]] >= 40 &
    x[["education.num"]] >= 13
  educsup <- c("Doctorate", "Prof-School", "Masters", "Bachelors")
  va_universitario <<- gsub(" ", "",x[["education"]]) %in% educsup
  posgra <- c("Doctorate", "Prof-School", "Masters")
  va_posgrado <<- gsub(" ", "",x[["education"]]) %in% posgra
  va_prof_pri <<- gsub(" ","",x[["occupation"]]) == "Prof-specialty" &
    gsub(" ","",x[["workclass"]]) == "Private" & 
    x[["age"]] < 40
  vsoles <- c("Divorced", "Never-married", "Separated")
  va_soles <<- gsub(" ","",x[["marital.status"]]) %in% vsoles & x[["age"]] > 40
  va_soles2 <<- gsub(" ","",x[["marital.status"]]) %in% vsoles & x[["age"]] <= 40
  va_campolab <<- gsub(".*gov","Estatal",x$workclass)
  va_campolab <<-gsub(".*Self.*","Self",va_campolab)
  va_campolab <<-gsub(".*Priv.*","Private",va_campolab)
  campolabva_ <<- as.factor(va_campolab)
  #TODO controlar div por cero
  va_lavepasar <<- (x[["capital.gain"]] + x[["capital.loss"]]) / x[["age"]]
  va_log_educ <<- log(x$education.num)
  #va_log_hxw <<- log(x$hours.per.week)
  #va_ruido <<- log(x$education.num * x$hours.per.week)
  #va_edad <<- cut(x$age, breaks = seq(10,100,10))
  va_entre4060 <<- x$age >=40 & x$age <=60 & gsub(" ", "",x[["education"]]) %in% educsup
}

metricas_a <- function(xtest, pred){
  print(MC <- table(test=xtest[, "class"],pred))
  precision <- MC[2,2] / (MC[2,2] + MC[1,2])
  recall <- MC[2,2] / (MC[2,2] + MC[2,1])
  c("precision" = precision, "recall" = recall)
}

# agrego variables va_...
# agrego como variables a test y train los objetos que se llamen va_...
#agregadas <- c("capital_neto", "afortunado", "tiempo_completo", "macho_alfa", "universitario",
#              "posgrado", "prof_pri", "soles", "campolab", "lavepasar")
# creo los objetos con los datos de train
generar(train0)
objs <- ls()
agregar <- objs[grep("^va_",objs)]
e <- globalenv()
train <- train0
for(j in 1:length(agregar)){
  train <- cbind(train, e[[agregar[j]]])
}
names(train) <- c(names(train0), agregar)
#train <- cbind(train0, capital_neto, afortunado, tiempo_completo, macho_alfa, universitario,
#                posgrado, prof_pri, soles, campolab,lavepasar, log_educ, log_hxw)
# creo los objetos con los datos de test
generar(test0)
objs <- ls()
agregar <- objs[grep("^va_",objs)]
e <- globalenv()
test <- test0
for(j in 1:length(agregar)){
  test <- cbind(test, e[[agregar[j]]])
}
names(test) <- c(names(test0), agregar)
#test <- cbind(test0, capital_neto, afortunado, tiempo_completo, macho_alfa, universitario,
#                posgrado, prof_pri, soles, campolab, lavepasar, log_educ, log_hxw)

train <- subset(train, select = -c(fnlwgt))
test <- subset(test, select = -c(fnlwgt))

# ejemplo original
library(randomForest)
features <- c("class","capital.gain", "age", "relationship", "marital.status","occupation")
train_rf <- train[,features]
test_rf <- test[,features]
set.seed(1)
Modelo_rf <- randomForest(class ~ ., train_rf, ntree = 100)
Prediccion_rf0 <- predict(Modelo_rf, test_rf)
metricas0 <- metricas_a(test_rf, Prediccion_rf0)

# agrego variables propuestas
features <- c("class","capital.gain", "age", "relationship", "marital.status","occupation", "va_capital_neto", "va_afortunado", 
              "va_tiempo_completo", "va_macho_alfa", "va_universitario", "va_posgrado")
train_rf1 <- train[,features]
test_rf1 <- test[,features]
set.seed(1)
Modelo_rf1 <- randomForest(class ~ ., train_rf1, ntree = 100)
Prediccion_rf1 <- predict(Modelo_rf1, test_rf1)
metricas1 <- metricas_a(test_rf1, Prediccion_rf1)

# agrego variables
features <- c("class", "workclass", "capital.gain", "age", "sex", "relationship", "marital.status","occupation",
              agregar)
features <- c("class", "capital.gain", "age", "sex","occupation",agregar)
train_rf2 <- train[,features]
test_rf2 <- test[,features]
set.seed(1)
Modelo_rf2 <- randomForest(class ~ ., train_rf2, ntree = 100, mtry = 2, replace = TRUE, type="class")
#Modelo_rf2 <- randomForest(class ~ ., train_rf2, ntree = 100, mtry = 4, replace = TRUE, cutoff = c(0.25,0.75))
Prediccion_rf2 <- predict(Modelo_rf2, test_rf2)
metricas2 <- metricas_a(test_rf2, Prediccion_rf2)

# a esta altura uso train y test con las variables propuestas y las que agregue

# regresion logistica (clase5)
train_glm <- train
test_glm <- test
set.seed(1)
Modelo_glm <- glm(class ~ ., train_glm, family = binomial(link = "logit"))
# Hace predicción y crea matriz de confusión
Prediccion_glm <- predict(Modelo_glm, newdata = test_glm, type = "response")
#Prediccion_glm <- round(Prediccion_glm) # Umbral = 0.5
Prediccion_glm <- Prediccion_glm > 0.56 # Umbral
#(MC <- table(test[, "class"],Prediccion))
metricas3 <- metricas_a(test_glm, Prediccion_glm)

# Crea modelo naive bayes (clase6)
library(e1071)
set.seed(1)
Modelo_nb <- naiveBayes(class ~., data=train)
Prediccion_nb <- predict(Modelo_nb, newdata = test)
metricas4 <- metricas_a(test, Prediccion_nb)

# modelo boosting
library(adabag)
set.seed(1)
modelo_ada <- boosting(class ~ ., data=train, mfinal=3)
Prediccion_ada <- predict(modelo_ada, test)$class
#(MC <- table(test[, "class"],Prediccion) )
metricas5 <- metricas_a(test, Prediccion_ada)

# Ensamble x voto
Prediccion_glm_ <- ifelse(Prediccion_glm,">50K","<=50K")
Prediccion_rf0_ <- gsub(" ","",Prediccion_rf0)
Prediccion_rf1_ <- gsub(" ","",Prediccion_rf1)
Prediccion_rf2_ <- gsub(" ","",Prediccion_rf2)
Prediccion_nb_ <- gsub(" ","",Prediccion_nb)
Prediccion_ada_ <- gsub(" ","",Prediccion_ada)
test_st <- cbind(test, Prediccion_rf0_, Prediccion_rf1_, Prediccion_rf2_, Prediccion_glm_, Prediccion_nb_, Prediccion_ada_ )
votos <- function(x, desempata){
  xt <- table(x)
  nxt <- names(xt)
  if(length(nxt) == 2){
    if(xt[nxt[1]] == xt[nxt[2]]){
      return(x[desempata])
    }
    if(xt[nxt[1]] > xt[nxt[2]]){
      return(nxt[1])
    } else{
      return(nxt[2])
    }
  } else{
    return(names(xt))
  }
}
# Sacando de la votacion a RF0 y RF1, empeora
# Sacando de la votacion a NB (la peor de las 6), tambien empeora
test_st$pred <- apply(test_st[, c("Prediccion_rf0_","Prediccion_rf1_","Prediccion_rf2_", "Prediccion_glm_", "Prediccion_nb_", "Prediccion_ada_")],1,
                      function(x){ votos(x,"Prediccion_rf2_")})
table(test_st[["class"]], test_st[["pred"]])
MC_ts <- table(test_st[["class"]], test_st[["pred"]])
precision <- MC_ts[2,2] / (MC_ts[2,2] + MC_ts[1,2])
recall <- MC_ts[2,2] / (MC_ts[2,2] + MC_ts[2,1])
metricas_FIN <- c("precision" = precision, "recall" = recall)
#precision    recall 
#0.8218673 0.5218409

print(metricas0)
print(metricas1)
print(metricas2)
print(metricas3)
print(metricas4)
print(metricas5)
print(agregar)
print(metricas_FIN)
